<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0-alpha1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-GLhlTQ8iRABdZLl6O3oVMWSktQOp6b7In1Zl3/Jr59b6EGGoI1aFkw7cmDA6j6gD" crossorigin="anonymous">
<div id='li' class="d-flex flex-column flex-shrink-0 p-3 bg-light" style="width: 280px;">
    <div href="" class="d-flex align-items-center mb-3 mb-md-0 me-md-auto link-dark text-decoration-none">
      <svg class="bi pe-none me-2" width="40" height="32"><use xlink:href="#bootstrap"></use></svg>
      <span class="fs-2">Menu</span>
</div>
    <ul class="nav nav-pills flex-column mb-auto">
      <li class="nav-item">
        <a href="/tortudev/" class="nav-link link-dark" aria-current="page">
          <svg class="bi pe-none me-2" width="16" height="16"></svg>
          <span>Accueil</span> 
        </a>
      </li>
      <li>
        <a href="/tortudev/histoire" class="nav-link link-dark">
          <svg class="bi pe-none me-2" width="16" height="16"></svg>
          <span>Histoire </span>
        </a>
      </li>
      <li>
        <a href="/tortudev/articles" class="nav-link link-dark">
          <svg class="bi pe-none me-2" width="16" height="16"></svg>
           <span>Articles</span>
        </a>
      </li>
    </ul>
    <hr>
 
  </div>