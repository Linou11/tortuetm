drop database if exists tortue;
create database tortue;
use tortue;

create table abonne (
    id int auto_increment primary key,
    nom varchar(255) not null,
    prénom varchar(255) not null,
    mail varchar(255) not null,
    password varchar(255) not null
);

create table article(
    id int auto_increment primary key,
    image text not null,
    titre varchar(255) not null,
    contenu text not null,
    size ENUM ('petit','moyen','grand') 
);

create table histoire(
    id int auto_increment primary key,
    image text not null,
    annee text not null,
    mois text not null,
    contenu text not null
);

create table admin (
    id int auto_increment primary key,
    nom varchar(255) not null,
    mail varchar(255) not null,
    pseudo varchar(255) not null,
    password varchar(255) not null
);
insert into admin(nom,mail,pseudo,password)
values (
    'admin',
    'admin@gmail.com',
    'admin',
    '089c4ecb692d6a2c41888f8a6a03908499495fbf3b14b76de57eeb77118663ee'
);
insert into histoire(annee, mois, contenu, image)
values (
    '2022',
    'Fevrier',
    'orem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industrys standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.',
    "https://picsum.photos/200"
    
);
insert into histoire(annee, mois, contenu, image)
values (
    '2022',
    'Fevrier',
    'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum bibendum vitae massa vitae vehicula. Nullam vel neque arcu. In metus mauris, viverra vel elementum sed, eleifend vitae mauris. Ut nec convallis quam. Vestibulum augue sapien, elementum nec dapibus vitae, dapibus quis nisi. Sed mi purus, suscipit at velit eu, interdum interdum justo. Vivamus convallis turpis eget nulla blandit laoreet eget vulputate ipsum. In mollis accumsan ornare. Sed quis odio magna. In posuere a lacus quis viverra. Nulla facilisi. Pellentesque placerat nunc nibh, vel fringilla elit sodales non',
    "https://picsum.photos/200"
);
insert into histoire(annee, mois, contenu, image)
values (
    '2020',
    'Fevrier',
    'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum bibendum vitae massa vitae vehicula. Nullam vel neque arcu. In metus mauris, viverra vel elementum sed, eleifend vitae mauris. Ut nec convallis quam. Vestibulum augue sapien, elementum nec dapibus vitae, dapibus quis nisi. Sed mi purus, suscipit at velit eu, interdum interdum justo. Vivamus convallis turpis eget nulla blandit laoreet eget vulputate ipsum. In mollis accumsan ornare. Sed quis odio magna. In posuere a lacus quis viverra. Nulla facilisi. Pellentesque placerat nunc nibh, vel fringilla elit sodales non',
    "https://picsum.photos/200"
);
insert into histoire(annee, mois, contenu, image)
values (
    '2018',
    'Fevrier',
    'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum bibendum vitae massa vitae vehicula. Nullam vel neque arcu. In metus mauris, viverra vel elementum sed, eleifend vitae mauris. Ut nec convallis quam. Vestibulum augue sapien, elementum nec dapibus vitae, dapibus quis nisi. Sed mi purus, suscipit at velit eu, interdum interdum justo. Vivamus convallis turpis eget nulla blandit laoreet eget vulputate ipsum. In mollis accumsan ornare. Sed quis odio magna. In posuere a lacus quis viverra. Nulla facilisi. Pellentesque placerat nunc nibh, vel fringilla elit sodales non',
    "https://picsum.photos/200"
);
insert into histoire(annee, mois, contenu, image)
values (
    '2019',
    'Fevrier',
    'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum bibendum vitae massa vitae vehicula. Nullam vel neque arcu. In metus mauris, viverra vel elementum sed, eleifend vitae mauris. Ut nec convallis quam. Vestibulum augue sapien, elementum nec dapibus vitae, dapibus quis nisi. Sed mi purus, suscipit at velit eu, interdum interdum justo. Vivamus convallis turpis eget nulla blandit laoreet eget vulputate ipsum. In mollis accumsan ornare. Sed quis odio magna. In posuere a lacus quis viverra. Nulla facilisi. Pellentesque placerat nunc nibh, vel fringilla elit sodales non',
    "https://picsum.photos/200"
);
insert into histoire(annee, mois, contenu, image)
values (
    '2022',
    'Fevrier',
    'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum bibendum vitae massa vitae vehicula. Nullam vel neque arcu. In metus mauris, viverra vel elementum sed, eleifend vitae mauris. Ut nec convallis quam. Vestibulum augue sapien, elementum nec dapibus vitae, dapibus quis nisi. Sed mi purus, suscipit at velit eu, interdum interdum justo. Vivamus convallis turpis eget nulla blandit laoreet eget vulputate ipsum. In mollis accumsan ornare. Sed quis odio magna. In posuere a lacus quis viverra. Nulla facilisi. Pellentesque placerat nunc nibh, vel fringilla elit sodales non',
    "https://picsum.photos/200"
    
);





drop user if exists toto@'127.0.0.1';
create user toto@'127.0.0.1' identified by 'mdp1234';
grant all privileges on tortue.* to toto@'127.0.0.1';
