<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>login</title>
    <link rel="stylesheet" href="addArt.css">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0-alpha1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-GLhlTQ8iRABdZLl6O3oVMWSktQOp6b7In1Zl3/Jr59b6EGGoI1aFkw7cmDA6j6gD" crossorigin="anonymous">
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0-alpha1/dist/js/bootstrap.bundle.min.js" integrity="sha384-w76AqPfDkMBDXo30jS1Sgez6pr3x5MlQ1ZAGC+nuZB+EYdgRZgiwxhTBTkF7CXvN" crossorigin="anonymous"></script>
</head>
<body>

    <?php

session_start();

if($_SESSION['admin']===1){ ?>
    <span>
        <form id="deco" action="log.php" method='post'>
            <input class="btn btn-danger" type="submit" value="Déconnexion">
        </form>
    </span>
    <div style="margin:20px" class='d-flex justify-content-center'>
        <h2 style="margin:30px" class="text-secondary">Vous êtes connecté en tant qu'administrateur</h2>
    </div>
    <div  style="height: 80px" class="d-flex justify-content-evenly">
        <a style="font-size:30px"  class='btn btn-outline-success' href="addArticle.php">
            [✚] Ajouter Article [✚]
        </a>
        <a style="font-size:30px" class='btn btn-outline-warning' href="../articles">
            [✍] Modifier Article [✍]
        </a>
    </div>
   
<?php
}else{
    
?>
    <main class="form-signin w-50 m-auto cent">
        <form action="log.php" class="forr w-100" method="post">
            <h1 class="h3 mb-3 fw-normal">Please sign in</h1>
        
            <div class="form-floating ">
            <input type="text" name="ID" class="form-control" id="floatingInput" placeholder="name@example.com">
            <label for="floatingInput">Votre ID</label>
            </div>
            <div class="form-floating">
            <input type="password" name="mdp" class="form-control w-100" id="floatingPassword" placeholder="Password">
            <label for="floatingPassword">Password</label>
            </div>
            <button  class="w-50 btn btn-lg btn-primary" type="submit">Sign in</button>
      </form>
    </main>  
   
    <?php }


?>
</body>
</html>