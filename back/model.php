<?php
include 'pdo.php';

function createArticle($contenu, $titre, $image,$format){
    global $pdo;
    $req = $pdo->prepare('insert into article (contenu, titre, image,size) values (?,?,?,?);');
    $req->execute([$contenu, $titre, $image,$format]);
};

function updateArticle($titre, $image, $contenu, $id, $format){
    global $pdo;
    $req = $pdo->prepare("update article set titre=?, image=?, contenu=? , size =? where id=?;");
    $req->execute([$titre, $image, $contenu,$format, $id]);
};

function readArticle($id){
    global $pdo;
    $req = $pdo->prepare("select * from article where id=?;");
    $req->execute([$id]);
    $repArticle=$req->fetchAll();
};

function createAbo($nom, $prenom, $mail, $password){
    global $pdo;
    $req = $pdo->prepare('insert into abonne (nom, prenom, mail, password) values (?,?,?,?);');
    $req->execute([$nom, $prenom, $mail, $password]);
};

function readAbo($id){
    global $pdo;
    $req = $pdo->prepare("select * from abonne where id=?;");
    $req->execute([$id]);
    $repAbo=$req->fetchAll();
};

?>