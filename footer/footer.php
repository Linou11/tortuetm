<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <link rel="stylesheet" href="/tortudev/footer/style.css">
    
</head>
<body>

<div class="headerf">
  <?php include 'wavef.php'; ?>


  <div class="coord">
Coordonnées
  </div>
<div class="row1 tot">
  <div class="col1">
    <div class="row1">
      <div><img class="imgpos" src="/tortudev/img/position.png"></div>
      <div class="margintxt txt2 body1">
        <p class="p">Ostal Occitan</p>
        <p class="p">31 rue Jean Jaurès</p>
        <p class="p">11100 Narbonne</p>
      </div>
      <div class="row1">
        <div class="divimg"><img class="imgrs" src="/tortudev/img/facebook.png"></div>
        <div class="divimg"><img class="imgrs" src="/tortudev/img/instagram.png"></div>
      </div>
    </div>
    <div class="row1 body1">
      <div class="jcenter"><img class="imgtel" src="/tortudev/img/tel.png"></div>
      <div class="row1 allcenter">
        <div class="margintxt allcenter txt2">
          <p class="p">06 07 53 00 91</p>
          <p class="p">Projet à vivre : François</p>
        </div>
        <div class="margintxt allcenter txt2">
          <p class="p">06.07.53.00.91</p>
          <p class="p">Ingénierie : Christine</p>
        </div>
      </div>
    </div>
  </div>
  <div class="col1">
    <div><h3 id='sloganBot'>Le vivant comme modèle</h3></div> 
    <div class="row1 divdis">
      <div class="jcenter margin txt">Si vous voulez discuter avec nous, ou pour tout autre question, n'hésitez pas à nous rejoindre.</div>
      <div><img class="imgdis" src="/tortudev/img/discord.png"></div>
    </div>
  </div>
</div>
<div class="row1">
  <div><img class="imgmail" src="/tortudev/img/mail.png"></div>
  <div class="marginmail">tortuemaraichere@gmail.com</div>
  
  <a href="/tortudev/mentions/mentions-légales.php" class="nav-link link-dark">
    <svg class="bi pe-none me-2" width="16" height="16"></svg>
    <span>Mentions légales</span>
  </a>
 
  <!-- <a href="/tortudev/mentions/politique-confidentialité.php" class="nav-link link-dark">
    <svg class="bi pe-none me-2" width="16" height="16"></svg>
      <span>Politique de confidentialité</span>
  </a> -->
</div>
</div>
</body>
</html>