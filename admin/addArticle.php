<head>
    
</head>
<?php include 'index.php';?>
<body>

    <div class="content">
   
    <?php
    //  include '../VIEW/navbar.php'; ?>

    <form id='add' action="../CONTROL/createArticle.php" method="post" enctype="multipart/form-data">

        <input type="hidden" name="id">
        <div class="radio">

            <input type="radio" class='btn-check' mxlg='220' name="format" id="peArtB" value='petit'>
            <label class="btn btn-outline-warning" for="peArtB" >
                <img id="peArt" src="../image/spritePA.png" alt="" srcset="">
                PETIT
            </label>
            
            <input type="radio" class='btn-check' name="format" mxlg='350' id="moArtB" value='moyen'>
            <label class="btn btn-outline-warning" for="moArtB">
                <img id="moArt" src="../image/spriteMA.png" alt="" srcset="">
                MOYEN
            </label>
            
            <input type="radio" class='btn-check' name="format" mxlg='500' id="grArtB" value='grand'>
            <label class="btn btn-outline-warning" for="grArtB">
                <img id="grArt" src="../image/spriteGA.png" alt="" srcset="">
                GRAND
            </label>
        </div>
        <div class="formTop">

            
            <input type="text" placeholder='Titre' class='form-control' name="titre" required>
    
            <label for="image">URL de l'image :</label>
            <input type="file" class='form-control form-control-lg' name="image">
            
        </div>

        <label for="contenu"></label>
        <textarea maxlength="4" name="contenu" class='form-control' placeholder="Contenu de l'article" id="txtt" cols="12" rows="4"></textarea>
    

        <input type="submit" class='btn btn-warning' value="Ajouter l'article">
    </form>
</div>
<script src='/tortudev/admin/lenght.js'></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0-alpha1/dist/js/bootstrap.bundle.min.js" integrity="sha384-w76AqPfDkMBDXo30jS1Sgez6pr3x5MlQ1ZAGC+nuZB+EYdgRZgiwxhTBTkF7CXvN" crossorigin="anonymous"></script>
</body>
</html>