<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <link rel="stylesheet" href="/tortudev/main/valeurs.css">
</head>
<body>
<div class="genPoly">
    <div class="poly1">
        <div class="polygone11 ">
            Eco Tourisme Durable
        </div>
    </div>
    <div class="poly2 media4 media6">
    <div class="mediaPad1">
    <div class="poly1">
        <div class="polygone11 ">
            Jardin citoyen
        </div>
    </div>
    </div>
        <div class="mediaPad2 mediaPad4">
        <div class="poly1">
        <div class="polygone11 ">
            Nouveaux Revenus Agricoles
        </div>
    </div>
        </div>
    </div>
    <div class="poly3 media4 media6">
    <div class="mediaPad1">
        <div class="poly1">
            <div class="polygone11 ">
           image 1
            </div>
        </div>
        </div>
        <div class="mediaPad2 mediaPad4">
        <div class="poly1">
        <div class="polygone11 ">
            image 2
        </div>
        </div>
        </div>
    </div>
    <div class="poly4 media4 media6">
            <div class="mediaPad1">
            <div class="poly1">
            <div class="polygone11 ">
                Chantiers d'insertion
            </div>
            </div>  
            </div>
        <div class="mediaPad2 mediaPad4">
                <div class="poly1">
        <div class="polygone11 ">
            Jardin citoyen
        </div>
    </div>
        </div>
    </div>
    <div class="imgPoly">
        <img src="/tortudev/main/imgPoly.png">
    </div>
    <div class="imgPoly2">
        <img src="/tortudev/main/imgPoly.png">
    </div>
    <div class="imgPoly3">
        <img src="/tortudev/main/imgPoly.png">
    </div>
    <div class="imgPoly4">
        <img src="/tortudev/main/imgPoly.png">
    </div>

</div>
</body>
</html>