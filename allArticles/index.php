<link rel="stylesheet" href="/tortudev/allArticles/moyenart.css">
<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0-alpha1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-GLhlTQ8iRABdZLl6O3oVMWSktQOp6b7In1Zl3/Jr59b6EGGoI1aFkw7cmDA6j6gD" crossorigin="anonymous">
<link rel="stylesheet" href="/tortudev/allArticles/art.css">
<link rel="stylesheet" href="/tortudev/allArticles/part.css">
<?php
session_start();
function moyen($titre,$image,$text,$id){
?>
<div class='moyAllPar'>
    <div class='moyall'>
        <div class='moycont'>
            <div class='zonetxt'>
                <h3><?= $titre?></h3>
                <p><?= $text?></p>     
            </div>
            <div class='imageMo'>
                <img src="/tortudev/img2/<?= $image ?>" alt="">
                <?php
                if($_SESSION['admin']===1){?>

                    <a class="btn btn-outline-warning" href="/tortudev/admin/updateArticle.php?id=<?=$id?>">Modifier</a>
                <?php }; ?>
            </div>
        </div>
        <div class='plant'>
            <img src="/tortudev/allArticles/img/plant.png" alt="">
        </div>
        <div class='sun'>
            <img src="/tortudev/allArticles/img/sun.png" alt="">
        </div>
        <div class='waveblack'>
            <img src="/tortudev/allArticles/img/waveblack.png" alt="">
        </div>
        <div class='moyred'>

        </div>
    </div>
</div>

<?php
};
function grand($titre,$image,$contenu, $id){
    ?>

      <div class="paddingGA">
          <div class="padd row">
     <div class="soleilGA">
     </div>
     <div class="carreGA">
     </div>
     <div class="GA row">
             <div class="col padGA">
                 <div class="titreGA">
                 <?= $titre ?>
                 </div>
                 <div class="txtGA">
                     <?= $contenu ?>
                     
                    </div>
                </div>
                <div class="col topGA">

                    <div class="imgGA">
                 <img class="imgGA100" src="tortudev/img2/<?= $image ?>">
                 <?php
                        if($_SESSION['admin']===1){?>

                            <a class="btn btn-outline-warning" href="/tortudev/admin/updateArticle.php?id=<?=$id?>">Modifier</a>
                        <?php }; ?>
                 </div>
                 <div class="barreGAH">
                     
                     </div>
                     <div class="barreGAB">
                         </div>
                        </div>
                    </div>
                </div>
            </div>
            <?php };

    ?>
    <?php 
    function petit($titre,$image,$contenu, $id){ ?>

  
    <div class='petitart'>
    <div class="plante"><!-- fleur -->
    </div>
    <div class="beige"><!-- carré beige -->

    </div>
    <div class="vague"><!-- vague -->
        <img src="/tortudev/image/vague.png">
    </div>
    <div class="carreTxt"> <!-- div texte -->
        <div class="text"><!-- texte -->
                <?= $contenu ?>
    </div>
    <div class="imgTxt"><!-- image texte -->
        <img src="/tortudev/image/imageText.png">
    </div>
    </div>
    <?php
                if($_SESSION['admin']===1){?>

                    <a class="btn btn-outline-warning" href="/tortudev/admin/updateArticle.php?id=<?=$id?>">Modifier</a>
                <?php }; ?>
</div>    
<?php }; ?>
    </body>
    </html>



