<?php  include '../back/pdo.php';  ?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Histoire</title>
    <link rel="stylesheet" href="histoire.css">
    <link href="https://fonts.googleapis.com/css2?family=Antonio:wght@100&display=swap" rel="stylesheet">

</head>
<body>
    <?php 
    include 'debug.php'
    include "../header/index.php";
    ?>
    <div id='h2top'>
        <h2 class=''>NOTRE HISTOIRE</h2>
        <p class='parahist'>De l’idée à la réalisation, d’une agriculture paysans terre et mer pour mieux vivre sur le littoral our les 20 ans qui viennent.</p>
    </div>
    
    <?php   
    $req = $pdo->query('select * from histoire;');
    $histoire = $req->fetchAll();
    // var_dump($histoire);
    $compte = 0;
    foreach($histoire as $his){
        if($compte === 0){
        ?>
      <img class="un"src="<?php echo $his['image']; ?>" alt="image">
      <img class="deux"src="<?php echo $his['image']; ?>" alt="image">
      <img class="trois"src="<?php echo $his['image']; ?>" alt="image">
      <?php } ?>

        <?php  if($compte === 1){
        ?>
        <div class="la2emedivdehistoire">
            <div class="entre2">
                <img class="carredroit"src="<?php echo $his['image']; ?>" alt="image">
                <img class="carregauche"src="<?php echo $his['image']; ?>" alt="image">
                <img class="carredroit"src="<?php echo $his['image']; ?>" alt="image">
                <img class="carregauche"src="<?php echo $his['image']; ?>" alt="image">
                <img class="carredroit"src="<?php echo $his['image']; ?>" alt="image">
            </div>
        </div>
        <div class="elemhistoire">
            <div class="date">
          <div> <p class='moishistoire'> <?php $arr2 = str_split($his['mois'], 3); echo $arr2[0]; ?> </p> </div>
          <div> <p class='anneehistoire'> <?php echo $his['annee']; ?> </p> </div>
          <div> <p class='moishistoire'> <?php $arr2 = str_split($his['mois'], 3); echo $arr2[0]; ?> </p> </div>
            </div>
            <div class="texthistoire">
                <p class='ptexthistoire'><?php echo $his['contenu']; ?></p>
            </div>
            </div>
            <?php } ?>

<?php  if($compte === 2){
        ?>
        <div class="toutespace">
            <div class="espace">
                <img class="wave1"src="image/wavescarte.png" alt="image">
                <img class="troisieme"src="<?php echo $his['image']; ?>" alt="image">
            </div>
            <div class="espace1">
                <img class="wave2"src="image/wavescarte.png" alt="image">
                <img class="troisieme1"src="<?php echo $his['image']; ?>" alt="image">
            </div>
        </div>
            <?php } ?>

            <?php  if($compte === 3){
        ?>  
        
            <div class="quatiemeelemphotodanshistoire">
                <div class="quatrime">
                    <img class="quatrime2" src="<?php echo $his['image']; ?>" alt="image">
                </div>
                <div class="quatrime1">
                   <img class="quatrime3" src="<?php echo $his['image']; ?>" alt="image">
                </div>
            </div>
            <?php $compte = -1; } ?>

        <?php if($compte != 1){ ?>
            <div class="elemhistoire">
        <div class="date">
          <div> <p class='moishistoire'> <?php $arr2 = str_split($his['mois'], 3); echo $arr2[0]; ?> </p> </div>
          <div class='anne'> <div class="boule"></div><div class="trait"></div><p class='anneehistoire'> <?php echo $his['annee']; ?> </p><div class="trait"></div><div class="boule"></div> </div>
          <div> <p class='moishistoire'> <?php $arr2 = str_split($his['mois'], 3); echo $arr2[0]; ?> </p> </div>
        </div>
          <div class="texthistoire"><p class='ptexthistoire'><?php echo $his['contenu']; ?></p></div>
        </div>
      <?php } $compte += 1 ;
    };
    include '../footer/footer.php';
    ?>
   
    
</body>
</html>