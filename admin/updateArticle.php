<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0-alpha1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-GLhlTQ8iRABdZLl6O3oVMWSktQOp6b7In1Zl3/Jr59b6EGGoI1aFkw7cmDA6j6gD" crossorigin="anonymous">
    <link rel="stylesheet" href="style.css">
    <link rel="stylesheet" href="addArt.css">
</head>
<body>

    <?php include '../back/readAll.php'; ?>
    
    <?php
    $art= readArt($_GET['id']);
    $page='';
    
    ?>
    <h2>Modifcation de L'article <?= $art['titre']?></h2>

    <div class="content">


        <form action="../CONTROL/updateArticle.php" method="post"  enctype="multipart/form-data">
            
            
            <input type="hidden" value="<?= $art['id']?>" name="id">
            <label for="titre">Titre :</label>
            <input type="text" name="titre" class="form-control form-control-lg" required value='<?= $art['titre']?>'>
            
                <label for="image">URL de l'image :</label>
                <input name="image" type="file" class='form-control form-control-lg'>

                <label for="contenu">Contenu de l'article :</label>
                <textarea name="contenu" class='form-control' id="" cols="30" rows="10"> <?= $art['contenu']?> </textarea>
                <div class="radio">

                    <input type="radio" class='btn-check' name="format" id="peArtB" value='petit'>
                    <label class="btn btn-outline-warning" for="peArtB" >
                        <img id="peArt" src="../image/spritePA.png" alt="" srcset="">
                        PETIT
                    </label>

                    <input type="radio" class='btn-check' name="format" id="moArtB" value='moyen'>                   
                    <label class="btn btn-outline-warning" for="moArtB">
                        <img id="moArt" src="../image/spriteMA.png" alt="" srcset="">
                        MOYEN
                    </label>
                    
                    <input type="radio" class='btn-check' name="format" id="grArtB" value='grand'>                   
                    <label class="btn btn-outline-warning" for="grArtB">
                        <img id="grArt" src="../image/spriteGA.png" alt="" srcset="">
                        GRAND
                    </label>
                </div>
                <input type="submit" class='btn btn-secondary' value="Modifier l'article">
            </form>
        </div>


<?php 
// include 'footer.php'; 
?>

<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0-alpha1/dist/js/bootstrap.bundle.min.js" integrity="sha384-w76AqPfDkMBDXo30jS1Sgez6pr3x5MlQ1ZAGC+nuZB+EYdgRZgiwxhTBTkF7CXvN" crossorigin="anonymous"></script>
</body>
</html>